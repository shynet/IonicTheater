var express = require('express');
var mongoose = require('mongoose');
var app = express();

var bodyParser = require('body-parser')
app.use(bodyParser());
app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

var ObjectId = require('mongodb').ObjectId;

var Comment = mongoose.model('comments', {
    name: String,
    content: String,
    date_creatd : Date,
});

var Movie = mongoose.model('movies', {
    name : String,
    releaseDate : Date,
    summary: String,
    likes: Number,
    likedMovie: Boolean,
    comments : [],
    coverUrl : String
});


// Connect to Mongo DB using mongoose
mongoose.connect("mongodb://Admin:Aa123456@testcluster-shard-00-00-ycncl.mongodb.net:27017,testcluster-shard-00-01-ycncl.mongodb.net:27017,testcluster-shard-00-02-ycncl.mongodb.net:27017/Theater?ssl=true&replicaSet=TestCluster-shard-0&authSource=admin")

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function() {
    console.log("Now connected to MongoDB");


    mongoose.model('movies').find(function(err, movies) {

        var coverUrls = ['advance-card-bttf.png', 'advance-card-jp.jpg', 'advance-card-tmntr.jpg'];

       for (mIndex in movies) {
           var movie = movies[mIndex];
           movie.coverUrl = coverUrls[Math.floor(Math.random() * coverUrls.length)];
           movie.save();
       }
    });

    /* ---> This code creates random movies <----
    var fs = require('fs');
    var obj = JSON.parse(fs.readFileSync('movies.json', 'utf8'));

    var randomSummaries = [
        'Just a random summary saying anything about the movie',
        'A magnificent movie about some weird stuff happening around the world',
        'A magical world of wizards in this thrilling action film by Stan Lee',
        'A world captical venture movie where Tom Hanks usually does what he do',
        'Nothing more special than a movie about crocodiles eating Axe, and other stuff',
        'Imagine a world of monsters hiding in evenry possible corner, waiting to strike',
        'A biography about Justin Bieber, including his fashino outlook and maturity',
        'The latest hit about Zombies! arrrggghhhhhhhhh!',
        'In this new Johnny Depp movie we can finally see Johnny Depp now wearing any costume...'
    ];


    var users = JSON.parse(fs.readFileSync('first_names.json', 'utf8'));
    var sentences = JSON.parse(fs.readFileSync('sentences.json', 'utf8'));


    for (var m in obj) {
         var movieObj = obj[m];

         var comments = [];

         for (var i = 0; i < Math.random() * 10; i++) {
             var comment = {
                 name : users[Math.floor(Math.random() * users.length)],
                 content : sentences[Math.floor(Math.random() * sentences.length)],
                 date_created : Date.now()
             };

             comments.push(comment);
         }

         var newMovie = Movie({
            name: movieObj.title,
            releaseDate: movieObj.releaseDate,
             summary: randomSummaries[Math.floor(Math.random() * randomSummaries.length)],
             likes: Math.floor(Math.random() * 1000),
             comments : comments,
             coverUrl : "",
         });

         newMovie.save();
    }
    */
});

app.get('/getMovies', function(req, res) {
    mongoose.model('movies').find(function(err, movies) {
        if (err == null) {
            res.json(movies);
        }
        else {
            res.json({ status : 'error' });
        }
    });
});

app.post('/likeMovie', function(req, res) {
    var movieId = req.body.id;
    var likedMovie = req.body.like == 'true';

    mongoose.model('movies').findOne(
        {
            "_id" : ObjectId(movieId)
        },
        function(err, movie) {
            if (err == null && movie != null) {
                if (movie.likedMovie == likedMovie)
                {
                    res.json({ status: 'error', error: 'You have already ' + (likedMovie ? 'liked' : 'unliked') + ' this movie!'});
                    return;
                }

                movie.likedMovie = likedMovie;
                movie.likes += (likedMovie) ? 1 : -1;
                movie.save();
                res.json({ status : 'ok '});
            }
            else
                res.json({ status : 'error' });
        });
});


app.post('/postComment', function(req, res) {
    var movieId = req.body.movieId;
    var user = req.body.user;
    var content = req.body.content;

    var comment = new Comment(
        {
            name : user,
            content : content,
            date_created : Date.now()
        });

    mongoose.model("movies").findOne({ _id : movieId }, function(err, movie) {
        movie.comments.push(comment);
        movie.save();
        return res.json(comment);
    });
});

var server = app.listen(8081, function() {

});
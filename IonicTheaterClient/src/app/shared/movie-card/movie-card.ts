import { Component, Input } from '@angular/core'
import {MoviesService} from "../../movies.service";

@Component({
  selector: 'movie-card',
  templateUrl: 'movie-card.html'
})
export class MovieCard {
  @Input() movie;

  @Input() showComments = false;
  commentContent;

  constructor(private moviesService:MoviesService) {

  }

  onCommentKeyDown($event) {
    if ($event.which == 13)
      this.postComment();
  }

  postComment() {
    this.moviesService.postComment(this.movie._id, "Guest", this.commentContent,
      (comment) => {
        this.movie.comments.push(comment)
    });

    this.commentContent = "";
  }

  commentsPressed($event) {
    this.showComments  = !this.showComments;
    $event.stopPropagation();
  }

  likePressed($event) {
    if (this.movie.likedMovie)
      this.unlikeMovie();
    else
      this.likeMovie();

    $event.stopPropagation();
  }

  likeMovie() {
    this.moviesService.likeMovie(this.movie._id, () => {
      this.movie.likedMovie = true;
      this.movie.likes += 1;
    });
  }

  unlikeMovie() {
    this.moviesService.unlikeMovie(this.movie._id, () => {
      this.movie.likedMovie = false;
      this.movie.likes -= 1;
    });
  }
}

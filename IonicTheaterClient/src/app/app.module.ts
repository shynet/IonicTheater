import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { MovieCard } from './shared/movie-card/movie-card';

import { AboutPage } from '../pages/about/about';
import { MoviePage } from '../pages/movie/movie';
import { NewMoviesPage } from '../pages/new-movies/new-movies';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {MoviesService} from "./movies.service";

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    MoviePage,
    NewMoviesPage,
    TabsPage,
    MovieCard
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    MoviePage,
    NewMoviesPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    MoviesService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

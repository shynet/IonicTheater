import { Injectable } from '@angular/core'
import { Http, Headers, RequestOptions, Response} from '@angular/http'

@Injectable()
export class MoviesService {
  movies;
  serviceUrl = "http://localhost:8081/";

  constructor(private http: Http) {

  }

  getEndpointUrl(endpoint) {
    return this.serviceUrl + endpoint;
  }

  getMovies(callback : Function) {
    let url = this.getEndpointUrl('getMovies');
    this.http.get(url).subscribe(
      (res:Response) =>
        {
          callback(res.json());
        }
    );
  }

  likeMovie(movieId, callback:Function) {
    this.likeMovieApi(movieId, true, callback);
  }

  unlikeMovie(movieId, callback:Function) {
    this.likeMovieApi(movieId, false, callback);
  }

  likeMovieApi(movieId, like, callback:Function) {
    let url = this.getEndpointUrl('likeMovie');



    this.http.post(url,
      'id=' + movieId + "&like=" + (like ? "true" : "false"), this.getFormRequestHeaders())
      .subscribe((res:Response) => {
        callback(res.json());
      })
  }

  postComment(movieId, user, content, callback:Function) {
    let url = this.getEndpointUrl('postComment');
    this.http.post(url, 'movieId=' + movieId + '&user=' + user + '&content=' + content, this.getFormRequestHeaders())
      .subscribe((res:Response) => {
        callback(res.json())
      })
  }

  getFormRequestHeaders() {
    let headers = new Headers();
    let requestOptions = new RequestOptions({ headers: headers });

    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return requestOptions;
  }
}

import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

import 'rxjs/add/operator/map';
import {MoviesService} from "../../app/movies.service";
import {MoviePage} from "../movie/movie";

@Component({
  selector: 'new-movies',
  templateUrl: 'new-movies.html'
})
export class NewMoviesPage {
  movies;

  constructor(private moviesService : MoviesService, public navCtrl: NavController, public navParams: NavParams) {
    this.moviesService.getMovies((movies) => {
      this.movies = movies;
    });
  }

  onMovieClick(movie) {
    this.navCtrl.push(MoviePage, {
      movie: movie
    });
  }
}

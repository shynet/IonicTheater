import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { NewMoviesPage } from '../new-movies/new-movies';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root = NewMoviesPage;
  tab2Root = AboutPage;

  constructor() {

  }
}

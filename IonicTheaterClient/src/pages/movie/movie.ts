import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

@Component({
  selector: 'page-movie',
  templateUrl: 'movie.html'
})
export class MoviePage {
  movie;

  constructor(public navCtrl: NavController, private navParams: NavParams) {
    this.movie = navParams.get("movie");
  }
}
